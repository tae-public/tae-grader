import requests
import doctest
import inspect
import io
import itertools
import json
import glob
import os
import random
import string
from contextlib import redirect_stderr, redirect_stdout
from jinja2 import Template
from textwrap import dedent

from .utils import hide_outputs
from pygments import highlight
from pygments.lexers import PythonConsoleLexer
from pygments.formatters import HtmlFormatter



def run_doctest(name, doctest_string, global_environment):
    """
    Run a single test with given global_environment.

    Returns (True, '') if the doctest passes.
    Returns (False, failure_message) if the doctest fails.
    """
    examples = doctest.DocTestParser().parse(
        doctest_string,
        name
    )
    test = doctest.DocTest(
        [e for e in examples if isinstance(e, doctest.Example)],
        global_environment,
        name,
        None,
        None,
        doctest_string
    )

    doctestrunner = doctest.DocTestRunner(verbose=True)

    runresults = io.StringIO()
    with redirect_stdout(runresults), redirect_stderr(runresults), hide_outputs():
        doctestrunner.run(test, clear_globs=False)
    with open(os.devnull, 'w') as f, redirect_stderr(f), redirect_stdout(f):
        result = doctestrunner.summarize(verbose=True)
    # An individual test can only pass or fail
    if result.failed == 0:
        return (True, '')
    else:
        return False, runresults.getvalue()

class OKTest:
    """
    A single DocTest defined by OKPy.

    Instances of this class are callable. When called, it takes
    a global_environment dict, and returns a TestResult object.

    We only take a global_environment, *not* a local_environment.
    This makes tests not useful inside functions, methods or
    other scopes with local variables. This is a limitation of
    doctest, so we roll with it.
    """
    result_pass_template = Template("""
    <p><strong>{{ name }}</strong> passed!</p>
    """)

    result_fail_template = Template("""
    <p><strong style='color: red;'>{{ name }}</strong></p>

    <p><strong>Test code:</strong><pre>{{test_code}}</pre></p>

    <p><strong>Test result:</strong><pre>{{test_result}}</pre></p>
    """)

    def __init__(self, name, tests, value=1):
        """
        tests is list of doctests that should be run.
        """
        self.name = name
        self.tests = tests
        self.value = value

    def run(self, global_environment):
        for i, t in enumerate(self.tests):
            passed, result = run_doctest(self.name + ' ' + str(i), t, global_environment)
            if not passed:
                return False, OKTest.result_fail_template.render(
                    name=self.name,
                    test_code=highlight(t, PythonConsoleLexer(), HtmlFormatter(noclasses=True)),
                    test_result=result
                )
        return True, OKTest.result_pass_template.render(name=self.name)

    # @classmethod
    # def from_source(cls, path: str):
    #     """
    #         Parse a ok test file & return an OKTest
    #     """
    #     from_file = True if path.endswith('.py') else False
    #
    #     # ok test files are python files, with a global 'test' defined
    #     test_spec = {}
    #     if from_file:
    #         test_globals = {}
    #         with open(path) as f:
    #             code = f.read()
    #             exec(code, test_globals)
    #         test_spec = test_globals['test']
    #     else:
    #         response = requests.get(f'https://l9afhy9a54.execute-api.us-west-2.amazonaws.com/dev/oktests/{path}')
    #         test_spec = response.json()
    #
    #     # We only support a subset of these tests, so let's validate!
    #
    #     # Make sure there is a name
    #     assert 'name' in test_spec
    #
    #     # Do not support multiple suites in the same file
    #     assert len(test_spec['suites']) == 1
    #
    #     test_suite = test_spec['suites'][0]
    #
    #     # Only support doctest. I am unsure if other tests are implemented
    #     assert test_suite.get('type', 'doctest') == 'doctest'
    #
    #     # Not setup and teardown supported
    #     assert not bool(test_suite.get('setup'))
    #     assert not bool(test_suite.get('teardown'))
    #
    #     tests = []
    #
    #     for i, test_case in enumerate(test_spec['suites'][0]['cases']):
    #         tests.append(dedent(test_case['code']))
    #
    #     return cls(path, tests, test_spec.get('points', 1))


class OKTests:
    def __init__(self, test_source_path):
        grade_rules = self.from_source(test_source_path)

        self.tests = [OKTest(rule['name'], [rule['code']], rule['points']) for rule in grade_rules['rules']]
        self.paths = [rule['name'] for rule in grade_rules['rules']]

    def from_source(self, path: str):
        """
            Parse a ok test file & return an OKTest
        """
        from_file = True if path.endswith('.py') else False

        # ok test files are python files, with a global 'test' defined
        grade_rules = {}
        if from_file:
            test_globals = {}
            with open(path) as f:
                code = f.read()
                exec(code, test_globals)
            grade_rules = test_globals['grade_rules']
        else:
            response = requests.get(f'https://l9afhy9a54.execute-api.us-west-2.amazonaws.com/dev/grules/{path}')
            grade_rules = response.json()

        return grade_rules

    def run(self, global_environment, include_grade=True):
        passed_tests = []
        failed_tests = []
        grade = 0
        total = 0
        for t in self.tests:
            total += t.value
            passed, hint = t.run(global_environment)
            if passed:
                grade += t.value
                passed_tests.append(t)
            else:
                failed_tests.append((t, hint))

        grade /= total

        return OKTestsResult(grade, self.paths, self.tests, passed_tests,
                             failed_tests, include_grade)


class OKTestsResult:
    """
    Displayable result from running OKTests
    """
    result_template = Template("""
    {% if include_grade %}
    <strong>Grade: {{ grade }}</strong>
    {% endif %}
    {% if grade == 1.0 %}
        <p>All tests passed!</p>
    {% else %}
        <p>{{ passed_tests|length }} of {{ tests|length }} tests passed</p>
        {% if passed_tests %}
        <p> <strong>Tests passed:</strong>
            {% for passed_test in passed_tests %} {{ passed_test.name }} {% endfor %}
        </p>
        {% endif %}
        {% if failed_tests %}
        <p> <strong>Tests failed: </strong>
            <ul>
            {% for failed_test, failed_test_hint in failed_tests %}
                <li> {{ failed_test_hint }} </li>
            {% endfor %}
            </ul>
        {% endif %}
    {% endif %}
    """)


    def __init__(self, grade, paths, tests, passed_tests, failed_tests, include_grade=True):
        self.grade = grade
        self.paths = paths
        self.tests = tests
        self.passed_tests = passed_tests
        self.failed_tests = failed_tests
        self.include_grade = include_grade

    def _repr_html_(self):
        return OKTestsResult.result_template.render(
            grade=self.grade,
            passed_tests=self.passed_tests,
            failed_tests=self.failed_tests,
            tests=self.tests,
            include_grade=self.include_grade
        )


def grade(test_source_path, global_env=None):
    """
    check global_env against given test_file in oktest format

    If global_env is none, the global environment of the calling
    function is used. The following two calls are equivalent:

    check('tests/q1.py')

    check('tests/q1.py', globals())

    Returns a TestResult object.
    """
    tests = OKTests(test_source_path)

    if global_env is None:
        # Get the global env of our callers - one level below us in the stack
        # The grade method should only be called directly from user / notebook
        # code. If some other method is calling it, it should also use the
        # inspect trick to pass in its parents' global env.
        global_env = inspect.currentframe().f_back.f_globals
    return tests.run(global_env, include_grade=False)
