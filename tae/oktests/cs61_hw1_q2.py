test = {
    'name': 'cs61_hw1_q2',
    'points': 2,
    'suites': [
        {
            'cases': [
                {
                    'code': r"""
          >>> characters_q1 == 1
          True
          """,
                    'hidden': False,
                    'locked': False
                },
                {
                    'code': r"""
          >>> characters_q2 == 2
          True
          """,
                    'hidden': False,
                    'locked': False
                }
            ],
            'scored': True,
            'setup': '',
            'teardown': '',
            'type': 'doctest'
        }
    ]
}
