test = {
    'name': 'np_arith1',
    'points': 5,
    'suites': [
        {
            'cases': [
                {
                    'code': r"""
          >>> print(first_product, second_product, third_product, fourth_product)
          6594 663168 6660320568 -39250
          """,
                    'hidden': False,
                    'locked': False
                },
            ],
            'scored': True,
            'setup': '',
            'teardown': '',
            'type': 'doctest'
        },
        {
            'cases': [
                {
                    'code': r"""
          >>> products
          array([      6594,     663168, 6660320568,     -39250])
          """,
                    'hidden': False,
                    'locked': False
                },
                {
                    'code': r"""
          >>> fixed_products
          array([      66234,     6661248, 66900162648,     -394250])
          """,
                    'hidden': False,
                    'locked': False
                },
                {
                    'code': r"""
          >>> # It looks like you multiplied -32 by 5/9, but not the array
          >>> # of temperatures.  Try using parentheses.
          >>> round(sum(celsius_max_temperatures)) != 3229921.0
          True
          >>> # It looks like you multipied and subtracted in the wrong
          >>> # order.
          >>> round(sum(celsius_max_temperatures)) != 356376.0
          True
          >>> round(sum(celsius_max_temperatures))
          1280821.0
          >>> len(celsius_max_temperatures)
          65000
          >>> celsius_max_temperatures.item(2003)
          20.0
          """,
                    'hidden': False,
                    'locked': False
                },
                {
                    'code': r"""
          >>> round(sum(celsius_temperature_ranges))
          768487.0
          >>> len(celsius_temperature_ranges)
          65000
          >>> celsius_temperature_ranges.item(1)
          10.0
          """,
                    'hidden': False,
                    'locked': False
                }
            ],
            'scored': True,
            'setup': '',
            'teardown': '',
            'type': 'doctest'
        }
    ]
}
