grade_rules = {
    'name': 'np_arith1',
    'rules': [
        {
            'name': 'q1',
            'points': 1,
            'code': r"""
          >>> print(first_product, second_product, third_product, fourth_product)
          6594 663168 6660320568 -39250
          """,
            'type': 'doctest'
        },
        {
            'name': 'q2',
            'points': 1,
            'code': r"""
          >>> products
          array([      6594,     663168, 6660320568,     -39250])
          """,
            'type': 'doctest'
        },
    ]
}
