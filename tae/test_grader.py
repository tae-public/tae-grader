import json
import os

from client.api.notebook import Notebook
from tae.grader import grade

defined_variable = None
seconds_in_a_minute = 60


def test_grade():
    test_code = 'np_arith1'
    file_base = os.path.dirname(__file__)
    test_file_path = os.path.join(file_base, 'grade_rules', f'{test_code}.py')

    grade(test_file_path)
    grade(test_code)


def test_upsert_test1():
    upsert_test('test1')

def test_upsert_cs61_hw1_q2():
    upsert_test('cs61_hw1_q2')


def test_upsert_np_arith1():
    upsert_test('np_arith1')


def upsert_test(test_source):
    url_base = 'https://l9afhy9a54.execute-api.us-west-2.amazonaws.com/dev/grules'
    file_base = os.path.dirname(__file__)

    test_file_path = os.path.join(file_base, 'grade_rules', f'{test_source}.py')
    test_url_path = os.path.join(url_base, test_source)

    testObject = {}
    with open(test_file_path) as f:
        code = f.read()
        exec(code, testObject)

    # jsonTest = json.dumps(testObject["grade_rules"], indent=4)
    # print()
    # print(jsonTest)

    import requests
    result = requests.put(test_url_path, json=testObject["grade_rules"])

    assert (result.status_code == 200)
