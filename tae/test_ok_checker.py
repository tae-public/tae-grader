import json
import os

from client.api.notebook import Notebook
from checker.ok_checker import check

defined_variable = None
seconds_in_a_minute = 60


def test_check():
    here = os.path.dirname(__file__)
    test_file_path = os.path.join(here, 'oktests/test1.py')
    check(test_file_path)

    test_code_path = 'test1'
    check(test_code_path)


def test_check_from_file():
    here = os.path.dirname(__file__)
    test_file_path = os.path.join(here, 'oktests/tests/test1.py')
    check(test_file_path)


def test_upsert_test1():
    upsert_test('test1')


def test_upsert_cs61_hw1_q2():
    upsert_test('cs61_hw1_q2')


def test_upsert_np_arith1():
    upsert_test('np_arith1')


def upsert_test(test_source):
    url_base = 'https://l9afhy9a54.execute-api.us-west-2.amazonaws.com/dev/oktests'
    file_base = os.path.dirname(__file__)

    test_file_path = os.path.join(file_base, 'oktests', f'{test_source}.py')
    test_url_path = os.path.join(url_base, test_source)

    testObject = {}
    with open(test_file_path) as f:
        code = f.read()
        exec(code, testObject)

    # jsonTest = json.dumps(testObject["test"], indent=4)
    # print(jsonTest)

    import requests
    result = requests.put(test_url_path, json=testObject["test"])

    assert (result.status_code == 200)
